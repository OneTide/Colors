﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buttons : MonoBehaviour {

	public Sprite layerBlue, layerRed;

	void OnMouseDown () {
		GetComponent<SpriteRenderer> ().sprite = layerRed;
	}

	void OnMouseUp () {
		GetComponent<SpriteRenderer> ().sprite = layerBlue;
	}

	void OnMouseUpAsButton () {
		switch (gameObject.name) {
		case "Play":
			Application.LoadLevel ("play");
			break;
		case "Rate":
			Application.OpenURL ("https://gitlab.com/OneTide/Colors");
			break;
		case "Replay":
			Application.LoadLevel ("play");
			break;
		case "Home":
			Application.LoadLevel ("main");
			break;
		}
	}
}